package com.chernov.loganalyzer.dto

import java.time.LocalDateTime
import java.util.UUID

data class LogPreviewDto(
    val name: String,
    val id: UUID?,
    val processed: LocalDateTime,
    val sizeByte: Int
) {
}