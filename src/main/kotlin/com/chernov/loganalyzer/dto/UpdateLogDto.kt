package com.chernov.loganalyzer.dto

import java.util.UUID

data class UpdateLogDto(
    val id: UUID,
    val name: String
) {
}