package com.chernov.loganalyzer.repository

import com.chernov.loganalyzer.entity.Log
import org.springframework.data.jpa.repository.JpaRepository
import java.util.UUID

interface LogRepository: JpaRepository<Log, UUID> {

}