package com.chernov.loganalyzer.repository

import com.chernov.loganalyzer.entity.LogEntry
import org.springframework.data.jpa.repository.JpaRepository
import java.util.UUID

interface LogEntryRepository: JpaRepository<LogEntry, UUID> {
}