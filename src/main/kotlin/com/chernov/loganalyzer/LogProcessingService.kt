package com.chernov.loganalyzer

import com.chernov.loganalyzer.command.UpdateLogCommand
import com.chernov.loganalyzer.entity.Log
import com.chernov.loganalyzer.entity.LogEntry
import com.chernov.loganalyzer.entity.LogLevel
import com.chernov.loganalyzer.repository.LogEntryRepository
import com.chernov.loganalyzer.repository.LogRepository
import org.mozilla.universalchardet.UniversalDetector
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.MultipartFile
import java.nio.charset.Charset
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.LinkedList
import java.util.UUID
import java.util.regex.Pattern

@Service
class LogProcessingService(
    private val logRepository: LogRepository,
    private val logEntryRepository: LogEntryRepository
) {
    companion object {
        private val LOG_HEADER_REGEX =
            Pattern.compile("(?<date>\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2},\\d{3}) (?<level>[A-Z]+) {1,2}(?<loggerName>[a-zA-Z0-9.]+) - (?<header>.+)")
    }

    @Transactional
    fun processLog(name: String, file: MultipartFile) {
        val logEntries = LinkedList<LogEntry>()

        val log = Log(name, file.bytes.size, "", LocalDateTime.now(), null, logEntries)

        val encoding = UniversalDetector.detectCharset(file.inputStream)

        val lines = file.inputStream.bufferedReader(Charset.forName(encoding)).readLines()

        var logEntry: LogEntry? = null

        for (line in lines) {
            val matcher = LOG_HEADER_REGEX.matcher(line)
            if (matcher.matches()) {
                logEntry = LogEntry(
                    line,
                    LogLevel.valueOf(matcher.group("level")),
                    LocalDateTime.parse(matcher.group("date"), DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss,nnn")),
                    matcher.group("loggerName"),
                    matcher.group("header"),
                    log
                )
                logEntries.add(logEntry)
            } else {
                if (logEntry != null) {
                   logEntry.originalContent = logEntry.originalContent.plus(line)
                   logEntry.message = logEntry.message.plus(line)
                }
            }
        }

        log.calculateStatistics()

        logRepository.save(log)
    }

    fun getLog(id: UUID): Log {
        return logRepository.getReferenceById(id)
    }

    fun getAllLogs(): List<Log> {
        return logRepository.findAll();
    }

    @Transactional
    fun updateLog(updateLogCommand: UpdateLogCommand) {
        val log: Log = logRepository.getReferenceById(updateLogCommand.id)
        log.name = updateLogCommand.name
        logRepository.save(log)
    }

    @Transactional
    fun deleteLog(id: UUID) {
        logRepository.deleteById(id)
    }
}