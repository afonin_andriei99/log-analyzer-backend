package com.chernov.loganalyzer.command

import com.chernov.loganalyzer.dto.UpdateLogDto
import java.util.UUID

data class UpdateLogCommand(
    val id: UUID,
    val name: String
) {
    constructor(updateLogDto: UpdateLogDto) : this(updateLogDto.id, updateLogDto.name)
}