package com.chernov.loganalyzer.entity

import jakarta.persistence.*
import java.time.LocalDateTime
import java.util.*

@Entity
class Log(
    var name: String,
    val sizeByte: Int,
    val originalFileId: String,
    val processedAt: LocalDateTime,
    val deletedAt: LocalDateTime?,
    @field:OneToMany(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    val entries: List<LogEntry>
) {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    var id: UUID? = null

    var infoCount: Int = 0
    var errorCount: Int = 0
    var failureRate: Double = 0.0
    var morningErrorPercent: Double = 0.0
    var dayErrorPercent: Double = 0.0
    var eveningErrorPercent: Double = 0.0

    companion object {
        private fun calculateErrorPercent(entries: List<LogEntry>, startHour: Int, endHour: Int): Double {
            val filteredEntries = entries.filter { logEntry -> logEntry.time.hour in startHour..endHour }
            return (filteredEntries
                        .filter { logEntry -> logEntry.level == LogLevel.ERROR }
                        .size
                        .toDouble() / filteredEntries.size.toDouble()) * 100.0
        }
    }

    fun calculateStatistics() {
        errorCount = entries.filter { logEntry -> logEntry.level == LogLevel.ERROR }.size
        infoCount = entries.filter { logEntry -> logEntry.level == LogLevel.INFO }.size
        failureRate = 1 - (errorCount.toDouble() / entries.size.toDouble())
        morningErrorPercent = calculateErrorPercent(entries, 6, 9)
        dayErrorPercent = calculateErrorPercent(entries, 10, 17)
        eveningErrorPercent = calculateErrorPercent(entries, 18, 23)
    }

}