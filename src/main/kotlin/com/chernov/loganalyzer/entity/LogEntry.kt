package com.chernov.loganalyzer.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.*
import org.hibernate.annotations.Type
import java.time.LocalDateTime
import java.util.*

@Entity
class LogEntry(
    @field:Column(columnDefinition = "TEXT")
    var originalContent: String,
    val level: LogLevel,
    val time: LocalDateTime,
    val loggerName: String,
    @field:Column(columnDefinition = "TEXT")
    var message: String,
    @field:ManyToOne
    @field:JsonIgnore
    val log: Log
) {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    var id: UUID? = null
}