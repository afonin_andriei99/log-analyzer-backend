package com.chernov.loganalyzer.controller

import com.chernov.loganalyzer.LogProcessingService
import com.chernov.loganalyzer.command.UpdateLogCommand
import com.chernov.loganalyzer.dto.LogPreviewDto
import com.chernov.loganalyzer.dto.UpdateLogDto
import com.chernov.loganalyzer.entity.Log
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import java.util.UUID

@RestController
class LogController(
    private val logProcessingService: LogProcessingService
) {

    @PostMapping("process")
    fun processLog(@RequestParam name: String, @RequestParam file: MultipartFile) {
        logProcessingService.processLog(name, file)
    }

    @GetMapping("log/{id}")
    fun getLog(@PathVariable id: UUID): Log {
        return logProcessingService.getLog(id)
    }


    @GetMapping("previews")
    fun getPreviews(): List<LogPreviewDto> {
        return logProcessingService.getAllLogs().map { LogPreviewDto(it.name, it.id, it.processedAt, it.sizeByte) }
    }

    @PostMapping("log/update")
    fun updateLog(@RequestBody updateLogDto: UpdateLogDto) {
        logProcessingService.updateLog(UpdateLogCommand(updateLogDto))
    }


    @DeleteMapping("log/{id}")
    fun deleteLog(@PathVariable id: UUID) {
        logProcessingService.deleteLog(id)
    }

}